var express = require('express');
var genres = require('./genres');

var server = express();

server.get('/', function(req, res) {
    res.send('');
});

server.get('/genres', function(req, res) {
    var data = genres.get();
    res.json(data);
});

server.listen(8000, function() {
    console.log("Run");
});