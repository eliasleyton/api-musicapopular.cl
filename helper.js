exports.getUrlData = function (u,v) {
    var match = RegExp('[?&]' + v + '=([^&]*)').exec(u);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}