var request = require('request');
var cheerio = require("cheerio");
var helper  = require("./helper");

exports.get = function () {

    base_url    = 'http://www.musicapopular.cl/3.0/';
    url         = "http://www.musicapopular.cl/3.0/index2.php?action=R2VuZXJv";
    genres = [];
    
    request(url, function (error, response, body) {
        if (!error) {
            $ = cheerio.load(body);
            var url, id, name;
            $('.notcolcentral3').each(function(i, elem) {
                url  = base_url + cheerio.load($(elem).html())('a').attr("href");                    
                id   = helper.getUrlData(url,'var');
                name = $(this).text();
                genres[i] = {
                    'id'    : id,
                    'name'  : name,
                    'url'   : url 
                }
            });
            return(genres);
        } else {
            return(error);
        }
    });
};
